return {
	color_scheme = "Gruvbox Dark",
	default_prog = {"sh.exe"},
	default_cwd = "C:/SGZ_Pro/",
	skip_close_confirmation_for_processes_named = {"sh.exe","bash.exe", "lf.exe"},
	font_size = 10.0
}
