" COLOR STUFF
" Does not work: set guifont=Hack:h12:cANSI:qDRAFT
set guifont=Consolas:h11
colorscheme evening
syntax on

" Plugins
execute pathogen#infect()
filetype plugin indent on


" Show number
set number

" Map leader
let mapleader = ","

" No backup stuff
set noswapfile
set nobackup

" Netrw
let g:netrw_banner = 0
let g:netrw_liststyle = 3
let g:netrw_winsize = 30
" Toggle Vexplore with Ctrl-E
function! ToggleVExplorer()
  if exists("t:expl_buf_num")
      let expl_win_num = bufwinnr(t:expl_buf_num)
      if expl_win_num != -1
          let cur_win_nr = winnr()
          exec expl_win_num . 'wincmd w'
          close
          exec cur_win_nr . 'wincmd w'
          unlet t:expl_buf_num
      else
          unlet t:expl_buf_num
      endif
  else
      exec '1wincmd w'
      Vexplore
      let t:expl_buf_num = bufnr("%")
  endif
endfunction
map <silent> <C-E> :call ToggleVExplorer()<CR>


" Make sure that when you get a error saying "Could not create folder .....
" Just create it"
autocmd BufWinLeave *.* mkview
autocmd BufWinEnter *.* loadview

highlight folded guibg=darkgrey
highlight folded guifg=yellow

" Disable the current stuff that is highlighted
map <silent> <C-h> :noh<cr>

" Searching but better
" Ignore case when searching
set ignorecase
" When searching try to be smart about cases
set smartcase
" Highlight search results
set nohlsearch
" Makes search act like search in modern browsers
set incsearch

" Source this file again.
noremap <silent> <leader>rc :source $HOME/.vimrc<CR>

"set cursorline
set cursorline
set cursorcolumn
highlight CursorLine ctermbg=Yellow cterm=bold guibg=#3b3b3b
highlight CursorColumn ctermbg=Yellow cterm=bold guibg=#3b3b3b

" Spell checker
map <leader>s :setlocal spell! spelllang=en_us<CR>
" Fix what the spell checker finds
noremap <silent> <leader>fix mc1z=`c

" Do space space to jump to <++>
noremap <Space><Space> /<++><Enter>"_c4l

" Make the letter after a period be capital :)
augroup CapitalAfterPeriods
  au!
  autocmd InsertCharPre * if search('\v(%^|[.!?]\_s)\_s*%#', 'bcnw') != 0 | let v:char = toupper(v:char) | endif
augroup END

" Replace stuff shortcut
nnoremap S :%s#g<Left><Left>

" File Tab stuffs
nnoremap <C-Left> :tabprevious<CR>
nnoremap <C-Right> :tabnext<CR>
nnoremap <silent> <A-Left> :tabm -1<CR>
nnoremap <silent> <A-Right> :tabm +1<CR>

" Copy paste
" NOTE: These are clipboard settings for windoze. Linux should be different.
set nopaste
set clipboard=unnamed

" Show text in middle
set sidescrolloff=999
set scrolloff=999

" NO BEEP
set noerrorbells visualbell t_vb=
autocmd GUIEnter * set visualbell t_vb=

" No more stinky esc key that's way to far away. Instead I can use alt + space.

nnoremap <M-Space> <esc>         " Remap in Normal mode
inoremap <M-Space> <Esc>         " Remap in Insert and Replace mode
vnoremap <M-Space> <esc>         " Remap in Visual and Select mode
xnoremap <M-Space> <esc>         " Remap in Visual mode
snoremap <M-Space> <esc>         " Remap in Select mode
cnoremap <M-Space> <C-C>         " Remap in Command-line mode
onoremap <M-Space> <esc>         " Remap in Operator pending mode

" Surperior jkl; rather then hjkl ALSO be able to go through wrapped text
" Never do this. This is a bad idea. If you use vim on the daily. All other
" programs have decided not to this so your screwed.
" noremap j h
" noremap k gj
" noremap l gk
" noremap ; l

" These autoclose stuff
" Also a bad idea. Don't do this because there in your way. On anny other
" program these things exist you just type {} left arrow and wow your there.
" It isn't that hard and you don't need them.
"inoremap ( ()<Esc>i
"inoremap { {}<Esc>i
"inoremap {<CR> {<CR>}<Esc>O
"inoremap [ []<Esc>i
"inoremap < <><Esc>i
" Never auto complete these when writing or doing anything. inoremap ' ''<Esc>i
" inoremap " ""<Esc>i

" Wildmenu
set wildmenu
set wildmode=longest,list,full

" Auto remove spaces on save
autocmd BufWritePre * %s/\s\+$//e

" Tab stuffs
" Use spaces instead of tabs
set expandtab
" Be smart when using tabs ;)
set smarttab
" 1 tab == 4 spaces
set shiftwidth=4
set tabstop=4
" Autoindents
"set autoindent
set smartindent
set backspace=indent,eol,start

" Status line
" Give some space before the line starts
set laststatus=2
" Actually start the status line parameters
set statusline=
" Show the file name
set statusline+=\ FILE:
set statusline+=\ %F
" Show both the current line number and the total lines in file
set statusline+=\ LINES:
set statusline+=\ %l
set statusline+=\ %L
" Go to the right side of the status line
set statusline+=%=
" Show the progress percentage of where you are in the document
set statusline+=%p

" Search duckduckgo
vmap gw "gy:silent! exec '!Start C:\Users\Stanl\AppData\Local\BraveSoftware\Brave-Browser\Application\brave.exe "duckduckgo.com/?q='@g'&ie=utf-8"'<CR><CR>dd

" LATEX AUTO
" Makes more latex stuff work with autocmd.
let g:tex_flavor ='tex'
" Insert a section
autocmd FileType tex inoremap ;sec \section{}<Enter><++><Enter><Esc>2kf}i
" Insert a begin
autocmd FileType tex inoremap ;beg \begin{DELRN}<Enter><++><Enter>\end{DELRN}<Esc>--www<Esc>:%s#DELRN##<Left>
" Command to delete all of what's in a {}
nnoremap <silent> @ ci{
" Insert a Frame
autocmd FileType tex inoremap ;fr \section{DELRN}<Enter>\begin{frame}<Enter>\frametitle{DELRN}<Enter><++><Enter>\end{frame}<Esc>
