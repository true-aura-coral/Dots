;; Package mirrors
(setq package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("org" . "https://orgmode.org/elpa/")
                         ("elpa" . "https://elpa.gnu.org/packages/")))


(package-initialize)
(unless package-archive-contents
 (package-refresh-contents))

;; Initialize use-package on non-Linux platforms
(unless (package-installed-p 'use-package)
   (package-install 'use-package))

(require 'use-package)
(setq use-package-always-ensure t)

;; Auto refresh when they get changed
(global-auto-revert-mode 1)

;; Stop making my directories filled with backups
(setq make-backup-files nil)
;; Text wrap everything
(add-hook 'text-mode-hook 'visual-line-mode)

;; Better scrolling
(setq scroll-step            1
      scroll-conservatively  10000)

;; Make ESC quit prompts
(global-set-key (kbd "<escape>") 'keyboard-escape-quit)

;; Set the default directory
(setq default-directory "C:/SGZ_Pro/" )

;; Don't beep
(setq visible-bell t)

;; remove some annoything UI in emacs
(scroll-bar-mode -1)        ; Disable visible scrollbar
(tool-bar-mode -1)          ; Disable the toolbar
(tooltip-mode -1)           ; Disable tooltips
(set-fringe-mode 10)        ; Give some breathing room
(menu-bar-mode -1)            ; Disable the menu bar

;; remove the ugly start menu
(setq inhibit-startup-message t)

;; Numbers
(column-number-mode)
(global-display-line-numbers-mode t)

;; Font
(add-to-list 'default-frame-alist '(font . "Dejavu Sans Mono-10"))
(set-face-attribute 'default t :font "DejaVu Sans Mono")

;;Time on your status bar
(setq display-time-24hr-format t)
(setq display-time-day-and-date t)
(setq display-time-default-load-average 'nil)
(display-time-mode 1)

(setq browse-url-browser-function 'browse-url-generic
          browse-url-generic-program "librewolf-portable")

;; Powerline status bar
(use-package powerline
  :ensure t)
(powerline-default-theme)

;; Doom theme
(use-package doom-themes
  :config
  (load-theme 'doom-gruvbox t))

;; Evil mode
(use-package undo-fu)
(use-package evil
  :init
  (setq evil-want-keybinding nil)
  (setq evil-undo-system 'undo-fu)
  :config
  (evil-mode 1))

(use-package evil-collection
  :after evil
  :config
  (evil-collection-init))

;; Org
(use-package org)
(setq org-src-preserve-indentation t)
;; God mode org setting
(setq org-startup-indented t)

;;org to html
(defun html-org-export-settings ()
  "Insert good html org settings"
  (interactive)
  (let ((user-input (completing-read "Enter a title here: " nil)))
  (insert (format "#+TITLE: %s\n#+HTML_HEAD: <link rel='stylesheet' type='text/css' href='styles.css' />\n#+OPTIONS: html-style:nil toc:nil num:nil \n\n* License\n#+BEGIN_EXPORT html \n<hr> \n<footer> \n<a rel='license' href='http://creativecommons.org/licenses/by-sa/4.0/'><img alt='Creative Commons License' style='border-width:0' width='88' height='31' src='../images/cc-by-sa.png' /></a><br> \nUnless otherwise noted, all content on this website is Copyright Zortazert 2021-2023 and is licensed under <a rel='license' href='http://creativecommons.org/licenses/by-sa/4.0/'>CC BY-SA 4.0</a>. \n</footer> \n#+END_EXPORT \n" user-input)))
  (forward-line -11)
  (end-of-line))

(use-package htmlize)
;;Org to tangle (Config)
(defun tangle-org-export-settings ()
  "Insert good tangle org settings"
  (interactive)
    (insert "#+TITLE: \n#+PROPERTY: header-args :tange ")
    (forward-line -1)
    (end-of-line))

;;Org to PDF
(defun pdf-org-export-settings ()
  "Insert good tangle org settings"
  (interactive)
    (insert "#+TITLE: \n#+OPTIONS: toc:nil author:nil\n#+DESCRIPTION: \n#+ATTR_LATEX: :environment longtable :align |p{3cm}|p{6cm}|l|l|l|l|l|l|")
    (forward-line -3)
    (end-of-line))

;; Org Capture

(setq org-capture-templates
      '(
	("j" "Journal" entry (file+datetree "C:/SGZ_Pro/Hobbys/Writings/Org/journal.org")
	 "* %?\n %i\n %a")
	)
      )

(use-package counsel
  :bind (("C-M-j" . 'counsel-switch-buffer)
         :map minibuffer-local-map
         ("C-r" . 'counsel-minibuffer-history))
  :custom
  (counsel-linux-app-format-function #'counsel-linux-app-format-function-name-only)
  :config
  (counsel-mode 1))

;; Ivy
(use-package ivy
  :diminish
  :bind (("C-s" . swiper)
         :map ivy-minibuffer-map
         ("TAB" . ivy-alt-done)
         ("C-l" . ivy-alt-done)
         ("C-j" . ivy-next-line)
         ("C-k" . ivy-previous-line)
         :map ivy-switch-buffer-map
         ("C-k" . ivy-previous-line)
         ("C-l" . ivy-done)
         ("C-d" . ivy-switch-buffer-kill)
         :map ivy-reverse-i-search-map
         ("C-k" . ivy-previous-line)
         ("C-d" . ivy-reverse-i-search-kill))
  :config
  (ivy-mode 1))

(setq ivy-initial-inputs-alist nil)

(use-package ivy-rich
  :init
  (ivy-rich-mode 1))

;; Variables for packages
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(newsticker-url-list
   '(("Pyero" "https://argo.codeberg.page/rss.xml" nil nil nil)
     ("Zortazert's Blog" "https://zortazert.codeberg.page/rss.xml" nil nil nil)
     ("davidovski" "https://davidovski.xyz/rss.xml" nil nil nil)
     ("Luke Smith's Webpage" "https://lukesmith.xyz/rss.xml" nil nil nil)
     ("beparanoid.de" "https://beparanoid.de/rss.xml" nil nil nil)
     ("TheLinuxCast Website" "https://thelinuxcast.org/feed/feed.xml" nil nil nil)
     ("WeedSmokingJew" "https://wsmj.neocities.org/index.xml" nil nil nil)
     ("Drew DeVault's blog" "https://drewdevault.com/blog/index.xml" nil nil nil)
     ("DenshiBlog" "https://denshi.org/rss.xml" nil nil nil)
     ("" "https://bugswriter.com/atom.xml" nil nil nil)
     ("Cadence's Blog" "https://cadence.moe/blog/rss.xml?limit=30" nil nil nil)
     ("Beans (Blog)" "https://thebeanbakery.xyz/rss.xml" nil nil nil)
     ("Jaiden Animations" "https://invidio.xamh.de/feed/channel/UCGwu0nbY2wSkW8N-cghnLpA" nil nil nil)
     ("TheOdd1sOut" "https://invidio.xamh.de/feed/channel/UCo8bcnLyZH8tBIH9V1mLgqQ" nil nil nil)
     ("GingerPale" "https://invidio.xamh.de/feed/channel/UCGGTAB19HlHEWPwwmxHsEKA" nil nil nil)
     ("Kevin Temmer Tunes" "https://invidio.xamh.de/feed/channel/UCR4u8Ny7c3V8qkvIF3JJG-Q" nil nil nil)
     ("illymation" "https://invidio.xamh.de/feed/channel/UCsKVP_4zQ877TEiH_Ih5yDQ" nil nil nil)
     ("Jazza" "https://invidio.xamh.de/feed/channel/UCHu2KNu6TtJ0p4hpSW7Yv7Q" nil nil nil)
     ("Young Yong Tales" "https://invidio.xamh.de/feed/channel/UCWPB0WpnMIy-g7zncwIhvQg" nil nil nil)
     ("Crowne Prince" "https://invidio.xamh.de/feed/channel/UCwf7GkXUML0Whgdt6lvMFTw" nil nil nil)
     ("Brandon James Greer" "https://invidio.xamh.de/feed/channel/UCC26K7LTSrJK0BPAUyyvtQg" nil nil nil)
     ("Xabio Arts" "https://invidio.xamh.de/feed/channel/UC7uTTvYKpjsDGRO_bcaMf7A" nil nil nil)
     ("Pontypants" "https://invidio.xamh.de/feed/channel/UCORaUhZVpg7yhxJeaPSTGmQ" nil nil nil)
     ("Vimlark" "https://invidio.xamh.de/feed/channel/UClqFSFR_H1yJJ7NRs8xxGCQ" nil nil nil)
     ("White Vault" "https://invidio.xamh.de/feed/channel/UC5-snCpm24G-mJvAbQP9HyA" nil nil nil)
     ("Dani" "https://invidio.xamh.de/feed/channel/UCIabPXjvT5BVTxRDPCBBOOQ" nil nil nil)
     ("Dani2" "https://invidio.xamh.de/feed/channel/UCzmtKpQQyZ-9ZNY_bbFtVHw" nil nil nil)
     ("DanisTutorials" "https://invidio.xamh.de/feed/channel/UCn7E5qFz-BML6o7XnjtlytA" nil nil nil)
     ("ThePrimeagen" "https://invidio.xamh.de/feed/channel/UC8ENHE5xdFSwx71u3fDH5Xw" nil nil nil)
     ("hnhx" "https://invidio.xamh.de/feed/channel/UCoQXr-stSbE69TB1eZtbd8w" nil nil nil)
     ("Jake@Linux" "https://invidio.xamh.de/feed/channel/UC1yGcBvdPGxRIMT1yo_bKIQ" nil nil nil)
     ("VickyTheChills" "https://invidio.xamh.de/feed/channel/UCexZJIiztSPe6chi2foArug" nil nil nil)
     ("Recent Commits to librex:main" "https://github.com/hnhx/librex/commits.atom" nil nil nil)
     ("Recent Commits to zet:main" "https://github.com/rwxrob/zet/commits.atom" nil nil nil)
     ("Feed of \"SimpleWeb/SimpleAmazon\"" "https://codeberg.org/SimpleWeb/SimpleAmazon.rss" nil nil nil)
     ("Pokemon News" "https://zortazert.codeberg.page/pokemon-news.xml" nil nil nil)
     ("Professor Alex Silver" "https://invidio.xamh.de/feed/channel/UC9PgkQgiREP0f_Or0ENUaGg" nil nil nil)
     ("Lumiose Trainer Zac" "https://invidio.xamh.de/feed/channel/UCAu71emkK4JWb3sEnqg9gOQ" nil nil nil)
     ("aDrive" "https://invidio.xamh.de/feed/channel/UCTrRj46ZQfPQoVREvCMmhwQ" nil nil nil)
     ("MunchingOrange" "https://invidio.xamh.de/feed/channel/UC7yMooezVAKSTHrL-pG3sWg" nil nil nil)
     ("Release notes from rss-bridge" "https://github.com/RSS-Bridge/rss-bridge/releases.atom" nil nil nil)
     ("MortMort 🍞 / @MortMort_" "https://nitter.42l.fr/mortmort_/rss" nil nil nil)
     ("Taran Van Hemert / @TaranVH" "https://nitter.42l.fr/TaranVH/rss" nil nil nil)
     ("@curvygurlflo" "https://bibliogram.jordanplayz158.xyz/u/curvygurlflo/rss.xml" nil nil nil)
     ("curvygurlflo" "https://proxitok.herokuapp.com/@curvygurlflo/rss" nil nil nil)
     ("CurvyGurlFlo" "https://invidio.xamh.de/feed/channel/UCXpU9UIedzYOgryYfS6TJHQ" nil nil nil)
     ("curvygurlflo - Instagram Bridge" "https://feed.eugenemolotov.ru/?action=display&bridge=InstagramBridge&context=Username&u=curvygurlflo&media_type=all&format=Atom" nil nil nil)
     ("Gus Johnson" "https://invidio.xamh.de/feed/channel/UCpIafFPGutTAKOBHMtGen7g" nil nil nil)
     ("Getgle" "https://invidio.xamh.de/feed/channel/UCtbXNvxdkAdNoUkLZ5YaDvg" nil nil nil)
     ("Language Simp" "https://invidio.xamh.de/feed/channel/UCYNyKRHBzd7UPRUtDhofUKg" nil nil nil)
     ("DenshiLive" "https://denshi.live/rss.xml" nil nil nil)
     ("Groomer Gaming" "https://vid.puffyan.us/feed/channel/UCzmKGGMyq82DW9merws7_mg" nil nil nil)
     ("Gonkee" "https://invidio.xamh.de/feed/channel/UCG2IoSJBUhrGL8fb5stMCWw" nil nil nil)))
 '(package-selected-packages
   '(lsp-mode org-visibility yaml-mode mastodon license-templates licence-templates emms-setup emms jabber magit quelpa-use-package rust-mode keycast all-the-icons counsel use-package ivy-rich evil-collection))
 '(send-mail-function 'smtpmail-send-it))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(use-package which-key
  :defer 0
  :diminish which-key-mode
  :config
  (which-key-mode)
  (setq which-key-idle-delay 1))

(use-package dashboard
  :ensure t
  :init
  (progn
    (setq dashboard-banner-logo-title "Welcome to Emacs! Be productive B)")
    (setq dashboard-startup-banner "C:/SGZ_Pro/Hobbys/Drawings/sphere-man.png")
    (setq dashboard-center-content t)
    (setq dashboard-items '((recents  . 30)
                            (bookmarks . 5)
                            (projects . 5)
                            (agenda . 5)))
    )
:config
(dashboard-setup-startup-hook))

(use-package elfeed
  :config
  :commands (elfeed)
  :bind ((:map elfeed-show-mode-map
	       ("p" . browse-url-with-mpv)))
  )
;; Allow opened entries to be in a bottom split
(setq elfeed-show-entry-switch 'display-buffer)
(setq elfeed-search-remain-on-entry t)

(defun browse-url-with-mpv (url &optional ignored)
  (interactive (browse-url-interactive-arg "URL: "))
  (call-process "mpv" nil 0 nil url))

;;Elfeed feed list
(setq elfeed-feeds (quote
		    (("https://denshi.org/rss.xml" website blog denshi)
		     ("https://lbry.bcow.xyz/@thelinuxcast:4/rss" thelinuxcast)
		     ("https://davidovski.xyz/rss.xml" iksvo)
		     ("https://lbry.bcow.xyz/@bugswriter:8/rss" bugswriter)
		     ("https://lbry.bcow.xyz/@DenshiVideo:f/rss?enclosure=true" denshi)
		     ("https://lukesmith.xyz/rss.xml" website blog luke)
		     ("https://lbry.bcow.xyz/@Luke:7/rss?enclosure=true" luke)
		     ("https://zortazert.codeberg.page/rss.xml" me)
		     ("https://lbry.bcow.xyz/@AlphaNerd:8/rss?enclosure=true" mentaloutlaw)
		     ("https://librarian.ix.tc/@DistroTube:2?enclosure=true" distrotube)
		     ("https://lbry.bcow.xyz/@GavinFreeborn:d?enclosure=true" gavinfreeborn)
		     ("https://lbry.bcow.xyz/@Odysee:8/rss" blog odysee)
		     ("https://lbry.bcow.xyz/@Odysee:8?enclosure=true" blog odysee)
		     ("https://lbry.bcow.xyz/@OdyseeHelp:b?enclosure=true" blog odysee)
		     ("https://lbry.bcow.xyz/@SystemCrafters:e/rss?enclosure=true" emacs systemcrafters)
		     ("https://lbry.bcow.xyz/@blenderdumbass:f/rss?enclosure=true" blenderdumbass)
		     ("https://lbry.bcow.xyz/@ArgoLargo:2/rss?enclosure=true" argolargo)
		     ("https://vid.puffyan.us/feed/channel/UCsn9MzwyPKeCE6MEGtMU4gg" mort)
		     ("https://nitter.42l.fr/mortmort_/rss" mort)
                     ("https://lbry.bcow.xyz/@vertbyqb:8/rss?enclosure=true" verty)
                     ("https://lbry.bcow.xyz/@lbry:3f/rss?enclosure=true" odysee blog lbry)
		     ("https://lbry.bcow.xyz/@Nasikla:5/rss?enclosure=true" naskila troler)
		     ("https://lbry.bcow.xyz/@BrodieRobertson:5/rss?enclosure=true" brodie))))

;; markdown stuff
(use-package markdown-mode
  :ensure t
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . markdown-mode)
         ("\\.markdown\\'" . markdown-mode))
  :init (setq markdown-command "multimarkdown"))

(setq ispell-program-name "C:/SGZ_Pro/z-Apps_Drivers/Hunspell/hunspell-1.3.2-3-w32-bin/bin/hunspell.exe")

;; Show keys being pressed
(use-package keycast)

;; Roooost
(use-package rust-mode)

;; gnus
(setq user-mail-address "trueauracoral@fedora.email"
      user-full-name "TrueAuraCoral")

(setq gnus-select-method
      '(nnimap "imaps"
               (nnimap-address "fedora.email")
               (nnimap-server-port "imaps")
               (nnimap-stream ssl)))

(setq smtpmail-smtp-server "fedora.email"
      smtpmail-smtp-service 587)
(setq gnus-message-archive-group "nnimap:Sent")

;; OLD ACCOUNT
;;(setq user-mail-address "zortazert@matthewevan.xyz"
;;      user-full-name "Zorta Zert")
;;
;;(setq gnus-select-method
;;      '(nnimap "imaps"
;;               (nnimap-address "mail.matthewevan.xyz")
;;               (nnimap-server-port "imaps")
;;               (nnimap-stream ssl)))
;;
;;(setq smtpmail-smtp-server "mail.matthewevan.xyz"
;;      smtpmail-smtp-service 587)
;;(setq gnus-message-archive-group "nnimap:Sent")

;; eww
(setq eww-search-prefix "https://search.davidovski.xyz/search.php?q=")
;; mastodon
;;(setq mastodon-instance-url "https://mastodon.online/")
;;(setq mastodon-activate-user "@trueauracoral")
;;(setq mastodon-auth-source-file "~/.emacs.d/masto")

;; magit
(use-package magit)

;; emojify
(use-package emojify)

(setf dired-kill-when-opening-new-dired-buffer t)

;; licence-templates
(use-package license-templates)

;; yaml
(use-package yaml-mode)

;; Very nice setting to make closing emacs much quicker.
(setq confirm-kill-processes nil)

;; org-visiblity
(use-package org-visibility
  :after (org)
  :demand t
  :bind* (:map org-visibility-mode-map
               ("C-x C-v" . org-visibility-force-save) ; defaults to `find-alternative-file'
               ("C-x M-v" . org-visibility-remove))    ; defaults to undefined
  :hook (org-mode . org-visibility-mode)
  :custom
  ;; list of directories and files to automatically persist and restore visibility state of
  (org-visibility-include-paths `(,(file-truename "C:/SGZ_Pro/Hobbys/Writing/Org")
                                  ,(file-truename "C:/SGZ_Pro/Hobbys/Writing/todo.org")))
  ;; list of directories and files to not persist and restore visibility state of
  (org-visibility-exclude-paths `(,(file-truename "~/org/old")
                                  ,(file-truename "~/org/test"))))

(use-package lsp-mode
  :commands (lsp lsp-deferred)
  ;; Add hooks for langs supported by lsp-mode
  :hook (lsp-mode . td/lsp-mode-setup)
  :init
  (setq lsp-keymap-prefix "C-c l")
  :config
  (lsp-enable-which-key-integration t))
